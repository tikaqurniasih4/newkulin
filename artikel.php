<?php
include  "header.php";
?>

		<!-- Main content Start -->
        <div class="main-content">
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="assets/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title">Blog Sidebar</h1>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>Blog Single</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->            

	      
            <!-- Blog Section Start -->
            <div class="rs-inner-blog orange-color pt-100 pb-100 md-pt-70 md-pb-70">
              <div class="container">
                  <div class="row">
                    <div class="col-lg-4 col-md-12 order-last">
                        <div class="widget-area">
                            <div class="search-widget mb-50">
                                <div class="search-wrap">
                                    <input type="search" placeholder="Searching..." name="s" class="search-input" value="">
                                    <button type="submit" value="Search"><i class=" flaticon-search"></i></button>
                                </div>
                            </div>
                            <div class="recent-posts-widget mb-50">
                                <h3 class="widget-title">Recent Posts</h3>
                                <div class="show-featured ">
                                    <div class="post-img">
                                        <a href="detail-artikel.php"><img src="assets/images/blog/style2/1.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="detail-artikel.php">Covid-19 threatens the next generation of smartphones</a>
                                        <span class="date">
                                            <i class="fa fa-calendar"></i>
                                             April 6, 2020
                                        </span>
                                    </div>
                                </div> 
                                <div class="show-featured ">
                                    <div class="post-img">
                                        <a href="detail-artikel.php"><img src="assets/images/blog/style2/2.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="detail-artikel.php">Soundtrack filma Lady Exclusive Music</a>
                                        <span class="date">
                                            <i class="fa fa-calendar"></i>
                                            November 19, 2018
                                        </span>
                                    </div>
                                </div> 
                                <div class="show-featured ">
                                    <div class="post-img">
                                        <a href="detail-artikel.php"><img src="assets/images/blog/style2/3.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="detail-artikel.php">Soundtrack filma Lady Exclusive Music </a>
                                        <span class="date">
                                            <i class="fa fa-calendar"></i>
                                            September 6, 2020
                                        </span>
                                    </div>
                                </div> 
                                <div class="show-featured ">
                                    <div class="post-img">
                                        <a href="detail-artikel.php"><img src="assets/images/blog/style2/4.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="detail-artikel.php">Given void great you’re good appear have i also fifth </a>
                                        <span class="date">
                                            <i class="fa fa-calendar"></i>
                                            September 6, 2020
                                        </span>
                                    </div>
                                </div> 
                                <div class="show-featured ">
                                    <div class="post-img">
                                        <a href="detail-artikel.php"><img src="assets/images/blog/style2/5.jpg" alt=""></a>
                                    </div>
                                    <div class="post-desc">
                                        <a href="detail-artikel.php">Lights winged seasons fish abundantly evening.</a>
                                        <span class="date">
                                            <i class="fa fa-calendar"></i>
                                             September 6, 2020
                                        </span>
                                    </div>
                                </div>
                            </div>
                         
                            <div class="widget-archives mb-50">
                                <h3 class="widget-title">Categories</h3>
                                <ul>
                                    <li><a href="detail-artikel.php">College</a></li>
                                    <li><a href="detail-artikel.php">High School</a></li>
                                    <li><a href="detail-artikel.php">Primary</a></li>
                                    <li><a href="detail-artikel.php">School</a></li>
                                    <li><a href="detail-artikel.php">University</a></li>
                                </ul>
                            </div>
                            
                        </div>
                    </div>
                      <div class="col-lg-8 pr-50 md-pr-15">
                          <div class="row">
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/1.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">University while the lovely valley team work</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">University</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/2.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">High school program starting soon 2021</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">High School</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/3.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">Modern School the lovely valley team work</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">Primary</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/4.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">While the lovely valley team work</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">College</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/5.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">This is a great source of content for anyone…</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">College</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/6.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">While the lovely valley team work</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">College</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12 mb-70">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/7.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">The Expenses You Are Thinking</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">School</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-12">
                                  <div class="blog-item">
                                      <div class="blog-img">
                                          <a href="detail-artikel.php"><img src="assets/images/blog/inner/8.jpg" alt=""></a>
                                      </div>
                                      <div class="blog-content">
                                          <h3 class="blog-title"><a href="detail-artikel.php">This is a great source of content for anyone…</a></h3>
                                          <div class="blog-meta">
                                              <ul class="btm-cate">
                                                  <li>
                                                      <div class="blog-date">
                                                          <i class="fa fa-calendar-check-o"></i> September 14, 2020                                                        
                                                      </div>
                                                  </li>
                                                  <li>
                                                      <div class="author">
                                                          <i class="fa fa-user-o"></i> admin  
                                                      </div>
                                                  </li>   
                                                  <li>
                                                      <div class="tag-line">
                                                          <i class="fa fa-book"></i>
                                                          <a href="detail-artikel.php">School</a> 
                                                      </div>
                                                  </li>
                                              </ul>
                                          </div>
                                          <div class="blog-desc">   
                                              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam...                                     
                                          </div>
                                          <div class="blog-button">
                                              <a class="blog-btn" href="#">Continue Reading</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div> 
              </div>
            </div>
            <!-- Blog Section End -->

           
        </div> 
        <!-- Main content End --> 
</div>
<!-- Main content End --> <?php
include  "footer.php";
?>