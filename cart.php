<?php
include  "header.php";
?>
            <!-- Breadcrumbs Start -->
            <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="assets/images/breadcrumbs/1.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text">
                    <h1 class="page-title">Keranjang Belanja</h1>
                    <ul>
                        <li>
                            <a class="active" href="index.html">Home</a>
                        </li>
                        <li>Keranjang</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->    
<!-- Cart Section Start -->
<div class="rs-cart orange-color pt-100 pb-100 md-pt-70 md-pb-70">
            <div class="container">
                <div class="cart-wrap">
                    <form>
                        <table class="cart-table">
                            <thead>
                                <tr>
                                    <!-- <th class="product-remove"></th> -->
                                    <th class="product-thumbnail"></th>
                                    <th class="product-thumbnail">Video/Tutor</th>
                                    <th class="product-name">Nama</th>
                                    <th class="product-price">Harga</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <!-- <td class="product-remove"><a href="#"><i class="fa fa-close"></i></a></td> -->
                                    <td class="product-thumbnail"><a href="#"><img src="assets/images/shop/4.jpg" alt=""></a></td>
                                   
                                    <td class="product-name"><a href="#">Medicine Bottle</a></td>
                                    <td class="product-price"><span>$</span>30.00</td>
                                   
                                </tr>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <!-- <td class="product-remove"><a href="#"><i class="fa fa-close"></i></a></td> -->
                                    <td class="product-thumbnail"><a href="#"><img src="assets/images/shop/4.jpg" alt=""></a></td>
                                    <td class="product-name"><a href="#">Medicine Bottle</a></td>
                                    <td class="product-price"><span>$</span>30.00</td>
                                   
                                </tr>
                                <tr>
                                    <!-- <td colspan="6" class="action text-end">
                                        <div class="coupon">
                                            <input type="text" name="text" placeholder="Coupon code" required="">
                                            <button type="submit" class="btn-shop apply-btn orange-color">apply coupon</button>
                                        </div>
                                        <div class="update-cart">
                                            <button class="btn-shop orange-color">Update cart</button>
                                        </div>
                                    </td> -->
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    <div class="cart-collaterals pt-70 md-pt-50">
                        <div class="cart-totals">
                            <h5 class="title mb-25">Cart totals</h5>
                            <table class="cart-total-table">
                                <tbody>
                                    <tr class="cart-subtotal">
                                        <th>Subtotal</th>
                                        <td><span class="amount"><span>$</span>60.00</span></td>
                                    </tr>
                                    <tr class="order-total">
                                        <th>Total</th>
                                        <td><strong><span class="amount"><span>$</span>60.00</span></strong></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="wc-proceed-to-checkout">
                               <a  href="checkout.php"> <button class="btn-shop orange-color"> Proceed to checkout</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Cart Section End --> 


        <?php
include  "footer.php";
?>