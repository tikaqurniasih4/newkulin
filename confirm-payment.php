<?php
include  "header.php";
?>

<!-- Checkout section start -->
<div id="rs-checkout" class="rs-checkout orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                 <div class="container">
                     <div class="coupon-toggle">
                         <div id="accordion" class="accordion">
                             <div class="card">
                                 <div class="card-header" id="headingOne">
                                     <!-- <div class="card-title">
                                         <span><i class="fa fa-window-maximize"></i> Have a coupon?</span>
                                         <button class="accordion-toggle" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Click here to enter your code</button>
                                     </div> -->
                                 </div>
                                 <!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                     <div class="card-body">
                                         <p>If you have a coupon code, please apply it below.</p>
                                         <div class="coupon-code-input">
                                             <input type="text" name="coupon_code" placeholder="Coupon code" required="">
                                         </div>
                                         <button class="btn-shop orange-color" type="submit">apply coupon</button>
                                     </div> 
                                 </div> -->
                             </div>
                         </div>
                     </div>

                     <div class="full-grid">
                     <div class="ordered-product">
                                 <div class="checkout-title">
                                     <h3>Konfirmasi Pembayaran</h3>
                                 </div>
                                 <table>
                                     <thead>
                                         <tr>
                                             <th class="w-50">Total</th>
                                             <th>Rp. 500.000</th>
                                         </tr>
                                     </thead>
                                    
                                 </table>

                                 <div class="form-content-box mt-5">
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                         <form>
                                             <div class="form-group">
                                                 <label>Bukti Pembayaran </label>
                                                 <input id="name" name="name" class="form-control-mod" type="file" required="">
                                             </div>
                                         </form>
                                        
                                         </div>
                                 </div>
                             </div><!-- .billing-fields -->

                                 <div class="bottom-area mb-5">
                                   <button class="btn-shop orange-color" type="submit">Konfirmasi</button>
                                 </div>
                             </div>

                        
                          
                 </div>
            </div>
            <!-- Checkout section end -->

            <?php
include  "footer.php";
?>