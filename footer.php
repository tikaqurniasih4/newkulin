<!-- Footer Start -->
<footer id="rs-footer" class="rs-footer home9-style home12-style">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <div class="footer-logo mb-30">
                                <a href="index.html"><img src="assets/images/logo-k.png" alt=""></a>
                            </div>
                             <!-- <div class="textwidget pr-60 md-pr-15"><p>We denounce with righteous indi gnation and dislike men who are so beguiled and demoralized by the charms of pleasure of your moment, so blinded by desire those who fail weakness.</p>
                              </div>-->
                              <ul class="footer_social ml-25">  
                                  <li> 
                                      <a href="#" target="_blank"><span><i class="fa fa-facebook green-color"></i></span></a> 
                                  </li>
                                  <li> 
                                      <a href="# " target="_blank"><span><i class="fa fa-twitter"></i></span></a> 
                                  </li>

                                  <li> 
                                      <a href="# " target="_blank"><span><i class="fa fa-pinterest-p"></i></span></a> 
                                  </li>
                                  <li> 
                                      <a href="# " target="_blank"><span><i class="fa fa-google-plus-square"></i></span></a> 
                                  </li>
                                  <li> 
                                      <a href="# " target="_blank"><span><i class="fa fa-instagram"></i></span></a> 
                                  </li>
                                                                           
                              </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 footer-widget md-mb-50">
                            <h3 class="widget-title">Address</h3>
                            <ul class="address-widget">
                                <li>
                                    <i class="flaticon-location"></i>
                                    <div class="desc">Yogyakarta</div>
                                </li>
                                <li>
                                    <i class="flaticon-call"></i>
                                    <div class="desc">
                                       <a href="tel:">680990084</a>
                                    </div>
                                </li>
                                <li>
                                    <i class="flaticon-email"></i>
                                    <div class="desc">
                                        <a href="mailto:officialkulin@gmail.com">officialkulin@gmail.com</a>
                                       
                                       
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4 col-md-12 col-sm-12 pl-50 md-pl-15 footer-widget md-mb-50">
                            <h3 class="widget-title">Courses</h3>
                            <ul class="site-map">
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Course Two</a></li>
                                <li><a href="#">Single Course</a></li>
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Login/Register</a></li>
                            </ul>
                        </div>
                        <!--  <div class="col-lg-3 col-md-12 col-sm-12 footer-widget">
                            <h3 class="widget-title">Recent Posts</h3>
                            <div class="recent-post mb-20">
                                <div class="post-img">
                                    <img src="assets/images/footer/1.jpg" alt="">
                                </div>
                                <div class="post-item">
                                    <div class="post-desc">
                                        <a href="#">University while the lovely valley team work</a>
                                    </div>
                                    <span class="post-date">
                                        <i class="fa fa-calendar"></i>
                                        September 20, 2020
                                    </span>
                                </div>
                            </div> 
                            <div class="recent-post mb-20 md-pb-0">
                                <div class="post-img">
                                    <img src="assets/images/footer/2.jpg" alt="">
                                </div>
                                <div class="post-item">
                                    <div class="post-desc">
                                        <a href="#">High school program starting soon 2021</a>
                                    </div>
                                    <span class="post-date">
                                       <i class="fa fa-calendar-check-o"></i>
                                        September 14, 2020
                                    </span>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">                    
                    <div class="row y-middle">
                        <div class="col-lg-6 md-mb-20">
                            <div class="copyright">
                                <p>&copy; 2020  KULIN - Kuliah & Kursus Online - All Rights Reserved.</a></p>
                            </div>
                        </div>
                        <div class="col-lg-6 text-end md-text-start">
                            <ul class="copy-right-menu">
                                <li><a href="#">Event</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->
        <!-- start scrollUp  -->
        <div id="scrollUp" class="green-color">
            <i class="fa fa-angle-up"></i>
        </div>
        <!-- End scrollUp  -->

        <!-- Search Modal Start -->
        <div class="modal fade search-modal" id="searchModal" tabindex="-1" aria-labelledby="searchModalLabel" aria-hidden="true">
            <button type="button" class="close" data-bs-dismiss="modal">
              <span class="flaticon-cross"></span>
            </button>
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="search-block clearfix">
                        <form>
                            <div class="form-group">
                                <input class="form-control" placeholder="Search Here..." type="text">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Search Modal End -->

        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">kgjkgdkugds</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- modernizr js -->
        <script src="assets/js/modernizr-2.8.3.min.js"></script>
        <!-- jquery latest version -->
        <script src="assets/js/jquery.min.js"></script>
        <!-- Bootstrap v5.0.2 js -->
        <script src="assets/js/bootstrap.min.js"></script>
        <!-- Menu js -->
        <script src="assets/js/rsmenu-main.js"></script> 
        <!-- op nav js -->
        <script src="assets/js/jquery.nav.js"></script>
        <!-- owl.carousel js -->
        <script src="assets/js/owl.carousel.min.js"></script>
        <!-- Slick js -->
        <script src="assets/js/slick.min.js"></script>
        <!-- isotope.pkgd.min js -->
        <script src="assets/js/isotope.pkgd.min.js"></script>
        <!-- imagesloaded.pkgd.min js -->
        <script src="assets/js/imagesloaded.pkgd.min.js"></script>
        <!-- wow js -->
        <script src="assets/js/wow.min.js"></script>
        <!-- Skill bar js -->
        <script src="assets/js/skill.bars.jquery.js"></script>
        <script src="assets/js/jquery.counterup.min.js"></script>        
         <!-- counter top js -->
        <script src="assets/js/waypoints.min.js"></script>
        <!-- video js -->
        <script src="assets/js/jquery.mb.YTPlayer.min.js"></script>
        <!-- magnific popup js -->
        <script src="assets/js/jquery.magnific-popup.min.js"></script>      
        <!-- plugins js -->
        <script src="assets/js/plugins.js"></script>
        <!-- contact form js -->
        <script src="assets/js/contact.form.js"></script>
        <!-- main js -->
        <script src="assets/js/main.js"></script>
         <!-- pick schedule js -->
         <script src="assets/js/pick-schedule/script.js"></script>


        
    </body>
</html>