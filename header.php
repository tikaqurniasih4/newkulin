<!DOCTYPE html>
<html lang="zxx">  
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title>KULIN - Kuliah & Kursus Online</title>
        <meta name="description" content="">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon
        <link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/logo-k.png"> 
        <!-- Bootstrap v5.0.2 css -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="assets/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="assets/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="assets/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="assets/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="assets/css/rsmenu-main.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="assets/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="defult-home">
        
        <!--Preloader area start here-->
        <div id="loader" class="loader">
            <div class="loader-container">
                <div class='loader-icon'>
                    <img src="assets/images/logo-k.png" alt="">
                </div>
            </div>
        </div>
        <!--Preloader area End here--> 


        <!--Full width header Start-->
        <div class="full-width-header header-style1 home1-modifiy home12-modifiy">
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Topbar Area Start -->
                <div class="topbar-area home11-topbar">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-md-5">
                                <ul class="topbar-contact">
                                    <li>
                                        <i class="flaticon-email"></i>
                                        <a href="mailto:officialkulin@gmail.com">officialkulin@gmail.com</a>
                                    </li>
                                    <li>
                                        <i class="fa flaticon-call"></i>
                                        <a href="tel:">098777779</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-7 text-end">
                                    <ul class="toolbar-sl-share">
                                        <!--<li class="opening"> <i class="flaticon-location"></i> Yogyakarta </li>-->
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Topbar Area End -->

               
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-lg-2">
                              <div class="logo-cat-wrap">
                                  <div class="logo-part">
                                      <a href="index.html">
                                          <img src="assets/images/logo-dark.png" alt="">
                                      </a>
                                  </div>
                              </div>
                            </div>
                            <div class="col-lg-8 text-end">
                                <div class="rs-menu-area">
                                    <div class="main-menu">
                                      <div class="mobile-menu">
                                          <a class="rs-menu-toggle">
                                              <i class="fa fa-bars"></i>
                                          </a>
                                      </div>
                                      <nav class="rs-menu">
                                         <ul class="nav-menu">
                                         <li class="single-item">
                                                 <a href="#">Home</a>
                                               
                                             </li>
                                            

                                             <!-- //.mega-menu -->
                                               <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> 
                                                <a href="index.html">Kuliah</a>
                                                <ul class="mega-menu"> 
                                                    <li class="mega-menu-container">
                                                        <div class="mega-menu-innner">
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu">
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                </ul>
                                                            </div>
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu last-sub-menu">
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                </ul>
                                                            </div>  
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu last-sub-menu">
                                                                <li><a href="">Category</a> </li>
                                                                    <li class="active"><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    <li><a href="">Category</a> </li>
                                                                    
                                                                </ul>
                                                            </div> 
                                                        </div>
                                                    </li>
                                                </ul> <!-- //.mega-menu -->
                                            </li>

                                             <!-- //.mega-menu -->
                                               <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> 
                                                <a href="index.html">Kursus</a>
                                                <ul class="mega-menu"> 
                                                    <li class="mega-menu-container">
                                                        <div class="mega-menu-innner">
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu">
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                </ul>
                                                            </div>
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu last-sub-menu">
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                </ul>
                                                            </div>  
                                                            <div class="single-megamenu">
                                                                <ul class="sub-menu last-sub-menu">
                                                                <li><a href="">Courses</a> </li>
                                                                    <li class="active"><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    <li><a href="">Courses</a> </li>
                                                                    
                                                                </ul>
                                                            </div> 
                                                        </div>
                                                    </li>
                                                </ul> <!-- //.mega-menu -->
                                            </li>
                                            
                                            <li class="single-item">
                                                 <a href="#">Contact</a>
                                               
                                             </li>
                                             <li class="single-item">
                                                 <a href="#">Artikel</a>
                                               
                                             </li>
                                             <li class="single-item">
                                                 <a href="#">Karir</a>
                                               
                                             </li>
                                             
                                             <!-- Kalau sudah login -->
                                             <li class="menu-item-has-children d-none">
                                                 <a href="#">Akun</a>
                                                 <ul class="sub-menu">
                                                    <!-- Kalau belum login -->
                                                    <li><a href="login.php">Login/Register</a> </li>
                                                    <!-- Kalau sudah login -->
                                                    <li><a href="profile.php">Hi, Tika!</a> </li>
                                                    <li><a href="history.php">Dashboard</a> </li>
                                                    <li><a href="logout.php">Logout</a> </li>
                                                   
                                                 </ul>
                                             </li>
                                            
                                            
                                             
                                         </ul> <!-- //.nav-menu -->
                                      </nav>                                         
                                    </div> <!-- //.main-menu -->                                
                                </div>
                            </div>
                            <div class="col-lg-2 text-end">
                                <div class="expand-btn-inner">
                                    <ul>
                                        <li>
                                            <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                <i class="flaticon-search"></i>
                                            </a>
                                        </li>
                                        <li class="icon-bar cart-inner no-border mini-cart-active hidden-xs">
                                            <a class="cart-icon">
                                                <!-- <span class="cart-count">2</span> -->
                                                <i class="fa fa-user"></i>
                                            </a>
                                            <div class="woocommerce-mini-cart text-start">
                                                <div class="cart-bottom-part">
                                                    <ul class="cart-icon-product-list">
                                                    <li class="display-flex">
                                                        <div class="product-info">
                                                                <a href="login.php">Login/Register</a>
                                                                
                                                            </div>
                                                         
                                                        </li>
                                                        <li class="display-flex">
                                                        <div class="product-info">
                                                                <a href="profile.php"> Hi, TIka!</a>
                                                                
                                                            </div>
                                                         
                                                        </li>
                                                        <li class="display-flex">
                                                            <div class="product-info">
                                                                <a href="history.php">Dashboard</a>
                                                            </div>
                                                        </li>
                                                        <li class="display-flex">
                                                            <div class="product-info">
                                                                <a href="logout.php">Logout</a>
                                                            </div>
                                                        </li>
                                                    </ul>

                                                    
                                                </div>
                                            </div> 
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End --> 

                <!-- Canvas Menu start
                <nav class="right_menu_togle hidden-md">
                    <div class="close-btn">
                        <div id="nav-close">
                            <div class="line">
                                <span class="line1"></span><span class="line2"></span>
                            </div>
                        </div>
                    </div>
                    <div class="canvas-logo">
                        <a href="index.html"><img src="assets/images/logo-dark.png" alt="logo"></a>
                    </div>
                    <div class="offcanvas-text">
                        <p>We denounce with righteous indige nationality and dislike men who are so beguiled and demo  by the charms of pleasure of the moment data com so blinded by desire.</p>
                    </div>
                    <div class="offcanvas-gallery">
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/1.jpg"><img src="assets/images/gallery/1.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/2.jpg"><img src="assets/images/gallery/2.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/3.jpg"><img src="assets/images/gallery/3.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/4.jpg"><img src="assets/images/gallery/4.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/5.jpg"><img src="assets/images/gallery/5.jpg" alt=""></a>
                        </div>
                        <div class="gallery-img">
                            <a class="image-popup" href="assets/images/gallery/6.jpg"><img src="assets/images/gallery/6.jpg" alt=""></a>
                        </div>
                    </div>
                    <div class="map-img">
                        <img src="assets/images/map.jpg" alt="">
                    </div>
                    <div class="canvas-contact">
                        <ul class="social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </nav> -->
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">