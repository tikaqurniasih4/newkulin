<?php
include  "header.php";
?>
<!-- Main content Start -->
<div class="main-content">
  <!-- Breadcrumbs Start -->
  <!-- <div class="rs-breadcrumbs breadcrumbs-overlay"><div class="breadcrumbs-img"><img src="assets/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image"></div><div class="breadcrumbs-text white-color"><h1 class="page-title">Learn  User Interface and User Experience</h1><ul><li><a class="active" href="index.html">Home</a></li><li>Course Details</li></ul></div></div> -->
  <!-- Breadcrumbs End -->
  <!-- Intro Courses -->
  <section class="intro-section gray-bg pt-94 pb-100 md-pt-64 md-pb-70">
    <div class="container">
      <div class="row clearfix">
        <!-- Content Column -->
        <div class="col-lg-12 md-mb-50">
          <!-- Intro Info Tabs-->
          <div class="intro-info-tabs">
            <ul class="nav nav-tabs intro-tabs tabs-box" id="myTab" role="tablist">
              <li class="nav-item tab-btns" role="presentation">
                <a class="nav-link tab-btn active" id="prod-schedule-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-schedule" role="tab" aria-controls="prod-schedule" aria-selected="true">Schedule</a>
              </li>
              <li class="nav-item tab-btns" role="presentation">
                <a class="nav-link tab-btn" id="prod-course-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-course" role="tab" aria-controls="prod-course" aria-selected="false">Course</a>
              </li>
              <li class="nav-item tab-btns" role="presentation">
                <a class="nav-link tab-btn" id="prod-video-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-video" role="tab" aria-controls="prod-video" aria-selected="false">Video</a>
              </li>
              <li class="nav-item tab-btns" role="presentation">
                <a class="nav-link tab-btn" id="prod-faq-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-faq" role="tab" aria-controls="prod-faq" aria-selected="false">Test</a>
              </li>
              <li class="nav-item tab-btns" role="presentation">
                <a class="nav-link tab-btn" id="prod-reviews-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-reviews" role="tab" aria-controls="prod-reviews" aria-selected="false">Payment</a>
              </li>
            </ul>
            <div class="tab-content tabs-content" id="myTabContent">
              <div class="tab-pane tab fade show active" id="prod-schedule" role="tabpanel" aria-labelledby="prod-schedule-tab">
                <div class="content white-bg pt-30">
                  <!-- Cource Overview -->
                  <div class="course-overview">
                    <div class="inner-box">
                      <div class="event-schedule-area-two bg-color pad100">
                        <div class="container">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="section-title text-center">
                                <div class="title-text">
                                  <h2>Courses Schedule</h2>
                                </div>
                                <p> In ludus latine mea, eos paulo quaestio an. Meis possit ea sit. Vidisse molestie <br /> cum te, sea lorem instructior at. </p>
                              </div>
                            </div>
                            <!-- /.col end-->
                          </div>
                          <!-- row end-->
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="tab-content" id="">
                                <div class="tab-pane fade active show" id="home" role="tabpanel">
                                  <div class="table-responsive">
                                    <table class="table">
                                      <thead>
                                        <tr>
                                          <th class="text-center" scope="col">Date</th>
                                          <th class="text-center" scope="col">Tutor</th>
                                          <th class="text-center" scope="col">Schedule</th>
                                          <th class="text-center" scope="col">Media</th>
                                          <th class="text-center" scope="col">Link Meet</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <tr class="inner-box">
                                          <th scope="row">
                                            <div class="event-date">
                                              <span>16</span>
                                              <p>Novembar</p>
                                            </div>
                                          </th>
                                          <td>
                                            <div class="event-img">
                                              <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" />
                                            </div>
                                          </td>
                                          <td>
                                            <div class="event-wrap">
                                              <h3>
                                                <a href="#">Course Title</a>
                                              </h3>
                                              <div class="meta">
                                                <div class="organizers">
                                                  <a href="#">Tutor Name</a>
                                                </div>
                                                <div class="categories">
                                                  <a href="#">
                                                    <span>05:35 AM - 08:00</span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="r-no">
                                              <span>Google Meet</span>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="primary-btn">
                                              <a class="btn btn-primary" href="#">Link Meet</a>
                                            </div>
                                          </td>
                                        </tr>
                                        <tr class="inner-box">
                                          <th scope="row">
                                            <div class="event-date">
                                              <span>16</span>
                                              <p>Novembar</p>
                                            </div>
                                          </th>
                                          <td>
                                            <div class="event-img">
                                              <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" />
                                            </div>
                                          </td>
                                          <td>
                                            <div class="event-wrap">
                                              <h3>
                                                <a href="#">Course Title</a>
                                              </h3>
                                              <div class="meta">
                                                <div class="organizers">
                                                  <a href="#">Tutor Name</a>
                                                </div>
                                                <div class="categories">
                                                  <a href="#">
                                                    <span>05:35 AM - 08:00</span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="r-no">
                                              <span>Google Meet</span>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="primary-btn">
                                              <a class="btn btn-primary" href="#">Link Meet</a>
                                            </div>
                                          </td>
                                        </tr>
                                        <tr class="inner-box">
                                          <th scope="row">
                                            <div class="event-date">
                                              <span>16</span>
                                              <p>Novembar</p>
                                            </div>
                                          </th>
                                          <td>
                                            <div class="event-img">
                                              <img src="https://bootdey.com/img/Content/avatar/avatar1.png" alt="" />
                                            </div>
                                          </td>
                                          <td>
                                            <div class="event-wrap">
                                              <h3>
                                                <a href="#">Course Title</a>
                                              </h3>
                                              <div class="meta">
                                                <div class="organizers">
                                                  <a href="#">Tutor Name</a>
                                                </div>
                                                <div class="categories">
                                                  <a href="#">
                                                    <span>05:35 AM - 08:00</span>
                                                  </a>
                                                </div>
                                              </div>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="r-no">
                                              <span>Google Meet</span>
                                            </div>
                                          </td>
                                          <td>
                                            <div class="primary-btn">
                                              <a class="btn btn-primary" href="#">Link Meet</a>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- /col end-->
                          </div>
                          <!-- /row end-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
 
              <div class="tab-pane fade" id="prod-course" role="tabpanel" aria-labelledby="prod-course-tab">
                <div class="content pt-30 white-bg">
                 <!-- Main content Start -->
        <div class="main-content">
           

            <!-- Popular Courses Section Start -->
            <div id="rs-popular-courses" class="rs-popular-courses style1 orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                   
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="courses-item mb-30">
                                <div class="img-part">
                                    <img src="assets/images/courses/1.jpg" alt="">
                                </div>
                                <div class="content-part">
                                    <ul class="meta-part">
                                        <li><span class="price">$55.00</span></li>
                                        <li><a class="categorie" href="#">Web Development</a></li>
                                    </ul>
                                    <h5 class="title"><a href="course-single.html">Become a PHP Master and Make Money Fast</a></h5>
                                    <div class="bottom-part">
                                        <div class="info-meta">
                                            <ul>
                                                <li class="user"><i class="fa fa-user"></i> 245</li>
                                                <li class="ratings">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    (05)
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-part">
                                            <a href="#"><i class="flaticon-right-arrow"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="courses-item mb-30">
                                <div class="img-part">
                                    <img src="assets/images/courses/1.jpg" alt="">
                                </div>
                                <div class="content-part">
                                    <ul class="meta-part">
                                        <li><span class="price">$55.00</span></li>
                                        <li><a class="categorie" href="#">Web Development</a></li>
                                    </ul>
                                    <h3 class="title"><a href="course-single.html">Become a PHP Master and Make Money Fast</a></h3>
                                    <div class="bottom-part">
                                        <div class="info-meta">
                                            <ul>
                                                <li class="user"><i class="fa fa-user"></i> 245</li>
                                                <li class="ratings">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    (05)
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-part">
                                            <a href="#"><i class="flaticon-right-arrow"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="courses-item mb-30">
                                <div class="img-part">
                                    <img src="assets/images/courses/1.jpg" alt="">
                                </div>
                                <div class="content-part">
                                    <ul class="meta-part">
                                        <li><span class="price">$55.00</span></li>
                                        <li><a class="categorie" href="#">Web Development</a></li>
                                    </ul>
                                    <h3 class="title"><a href="course-single.html">Become a PHP Master and Make Money Fast</a></h3>
                                    <div class="bottom-part">
                                        <div class="info-meta">
                                            <ul>
                                                <li class="user"><i class="fa fa-user"></i> 245</li>
                                                <li class="ratings">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    (05)
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-part">
                                            <a href="#"><i class="flaticon-right-arrow"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="courses-item mb-30">
                                <div class="img-part">
                                    <img src="assets/images/courses/1.jpg" alt="">
                                </div>
                                <div class="content-part">
                                    <ul class="meta-part">
                                        <li><span class="price">$55.00</span></li>
                                        <li><a class="categorie" href="#">Web Development</a></li>
                                    </ul>
                                    <h3 class="title"><a href="course-single.html">Become a PHP Master and Make Money Fast</a></h3>
                                    <div class="bottom-part">
                                        <div class="info-meta">
                                            <ul>
                                                <li class="user"><i class="fa fa-user"></i> 245</li>
                                                <li class="ratings">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    (05)
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-part">
                                            <a href="#"><i class="flaticon-right-arrow"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       
                        
                    </div>
                    <div class="pagination-area orange-color text-center mt-30 md-mt-0">
                        <ul class="pagination-part">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">Next <i class="fa fa-long-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Popular Courses Section End -->
              </div>
              </div>
              <div class="tab-pane fade" id="prod-video" role="tabpanel" aria-labelledby="prod-video-tab">
                <div class="content">
                  <div id="accordion" class="accordion-box">
                    <div class="card accordion block">
                      <div class="card-header" id="headingOne">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">UI/ UX Introduction</button>
                        </h5>
                      </div>
                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">
                        <div class="card-body acc-content current">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a class="popup-videos play-icon" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                                  <i class="fa fa-play"></i>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card accordion block">
                      <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Color Theory</button>
                        </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-bs-parent="#accordion">
                        <div class="card-body acc-content">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card accordion block">
                      <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Basic Typography</button>
                        </h5>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-bs-parent="#accordion">
                        <div class="card-body acc-content">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="prod-instructor" role="tabpanel" aria-labelledby="prod-instructor-tab">
                <div class="content pt-30 pb-30 pl-30 pr-30 white-bg">
                  <h3 class="instructor-title">Instructors</h3>
                  <div class="row rs-team style1 orange-color transparent-bg clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                      <div class="team-item">
                        <img src="assets/images/team/1.jpg" alt="">
                        <div class="content-part">
                          <h4 class="name">
                            <a href="#">Jhon Pedrocas</a>
                          </h4>
                          <span class="designation">Professor</span>
                          <ul class="social-links">
                            <li>
                              <a href="#">
                                <i class="fa fa-facebook"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-twitter"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-linkedin"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-google-plus"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                      <div class="team-item">
                        <img src="assets/images/team/2.jpg" alt="">
                        <div class="content-part">
                          <h4 class="name">
                            <a href="#">Jhon Pedrocas</a>
                          </h4>
                          <span class="designation">Professor</span>
                          <ul class="social-links">
                            <li>
                              <a href="#">
                                <i class="fa fa-facebook"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-twitter"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-linkedin"></i>
                              </a>
                            </li>
                            <li>
                              <a href="#">
                                <i class="fa fa-google-plus"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="prod-faq" role="tabpanel" aria-labelledby="prod-faq-tab">
                <div class="content">
                  <div id="accordion3" class="accordion-box">
                    <div class="card accordion block">
                      <div class="card-header" id="headingSeven">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">UI/ UX Introduction</button>
                        </h5>
                      </div>
                      <div id="collapseSeven" class="collapse show" aria-labelledby="headingSeven" data-bs-parent="#accordion3">
                        <div class="card-body acc-content current">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a class="popup-videos play-icon" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                                  <i class="fa fa-play"></i>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card accordion block">
                      <div class="card-header" id="headingEight">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn collapsed" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">Color Theory</button>
                        </h5>
                      </div>
                      <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-bs-parent="#accordion3">
                        <div class="card-body acc-content">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card accordion block">
                      <div class="card-header" id="headingNine">
                        <h5 class="mb-0">
                          <button class="btn btn-link acc-btn collapsed" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">Basic Typography</button>
                        </h5>
                      </div>
                      <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-bs-parent="#accordion3">
                        <div class="card-body acc-content">
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play">
                                    <i class="ripple"></i>
                                  </span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                          <div class="content">
                            <div class="clearfix">
                              <div class="pull-left">
                                <a href="https://www.youtube.com/watch?v=kxPCFljwJws" class="popup-videos play-icon">
                                  <span class="fa fa-play"></span>What is UI/ UX Design? </a>
                              </div>
                              <div class="pull-right">
                                <div class="minutes">35 Minutes</div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="prod-reviews" role="tabpanel" aria-labelledby="prod-reviews-tab">
                <div class="content pt-30 pb-30 white-bg">
                  <div class="cource-review-box mb-30">
                    <h4>Stephane Smith</h4>
                    <div class="rating">
                      <span class="total-rating">4.5</span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>&ensp; 256 Reviews
                    </div>
                    <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                    <div class="helpful">Was this review helpful?</div>
                    <ul class="like-option">
                      <li>
                        <i class="fa fa-thumbs-o-up"></i>
                      </li>
                      <li>
                        <i class="fa fa-thumbs-o-down"></i>
                      </li>
                      <li>
                        <a class="report" href="#">Report</a>
                      </li>
                    </ul>
                  </div>
                  <div class="cource-review-box mb-30">
                    <h4>Anna Sthesia</h4>
                    <div class="rating">
                      <span class="total-rating">4.5</span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>&ensp; 256 Reviews
                    </div>
                    <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                    <div class="helpful">Was this review helpful?</div>
                    <ul class="like-option">
                      <li>
                        <i class="fa fa-thumbs-o-up"></i>
                      </li>
                      <li>
                        <i class="fa fa-thumbs-o-down"></i>
                      </li>
                      <li>
                        <a class="report" href="#">Report</a>
                      </li>
                    </ul>
                  </div>
                  <div class="cource-review-box mb-30">
                    <h4>Petey Cruiser</h4>
                    <div class="rating">
                      <span class="total-rating">4.5</span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>&ensp; 256 Reviews
                    </div>
                    <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                    <div class="helpful">Was this review helpful?</div>
                    <ul class="like-option">
                      <li>
                        <i class="fa fa-thumbs-o-up"></i>
                      </li>
                      <li>
                        <i class="fa fa-thumbs-o-down"></i>
                      </li>
                      <li>
                        <a class="report" href="#">Report</a>
                      </li>
                    </ul>
                  </div>
                  <div class="cource-review-box">
                    <h4>Rick O'Shea</h4>
                    <div class="rating">
                      <span class="total-rating">4.5</span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>
                      <span class="fa fa-star"></span>&ensp; 256 Reviews
                    </div>
                    <div class="text">Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus.</div>
                    <div class="helpful">Was this review helpful?</div>
                    <ul class="like-option">
                      <li>
                        <i class="fa fa-thumbs-o-up"></i>
                      </li>
                      <li>
                        <i class="fa fa-thumbs-o-down"></i>
                      </li>
                      <li>
                        <a class="report" href="#">Report</a>
                      </li>
                    </ul>
                    <a href="#" class="more">View More</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End intro Courses -->
</div>
<!-- Main content End --> <?php
include  "footer.php";
?>