<?php
include  "header.php";
?>
             <!-- Slider Section Start -->
            <div class="rs-slider style1">
                <div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="1" data-md-device-nav="true" data-md-device-dots="false">
                    <div class="slider-content slide1">
                        <div class="container">
                            <div class="sl-sub-title white-color wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms"></div>
                            <!-- <h1 class="sl-title white-color wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Educavo University</h1>
                            <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                                <a class="readon2 banner-style" href="#">Discover More</a>
                            </div>-->
                        </div>
                    </div>
                    <div class="slider-content slide2">
                        <div class="container">
                           <!-- <h1 class="sl-title white-color wow fadeInRight" data-wow-delay="600ms" data-wow-duration="2000ms">Educavo University</h1>
                            <div class="sl-btn wow fadeInUp" data-wow-delay="900ms" data-wow-duration="2000ms">
                                <a class="readon2 banner-style" href="#">Discover More</a>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Slider Section End -->

            
            <div id="rs-about" class="rs-about style1 pb-100 md-pb-70">
                <div class="container">
                    <div class="row">
                    <!-- About Section Start 
                        <div class="col-lg-4 order-last">
                            <div class="notice-bord style1">
                                <h4 class="title">Notice Board</h4>
                                <ul>
                                    <li class="wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                                        <div class="date"><span>20</span>June</div>
                                        <div class="desc">Lorem Ipsum is simply dummy text of the printing and setting</div>
                                    </li>
                                    <li class="wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">
                                        <div class="date"><span>22</span>Aug</div>
                                        <div class="desc">Lorem Ipsum is simply dummy text of the printing and setting</div>
                                    </li>
                                    <li class="wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                                        <div class="date"><span>14</span>May</div>
                                        <div class="desc">Lorem Ipsum is simply dummy text of the printing and setting</div>
                                    </li>
                                    <li class="wow fadeInUp" data-wow-delay="600ms" data-wow-duration="2000ms">
                                        <div class="date"><span>31</span>Sept</div>
                                        <div class="desc">Lorem Ipsum is simply dummy text of the printing and setting</div>
                                    </li>
                                    <li class="wow fadeInUp" data-wow-delay="700ms" data-wow-duration="2000ms">
                                        <div class="date"><span>28</span>Oct</div>
                                        <div class="desc">Lorem Ipsum is simply dummy text of the printing and setting</div>
                                    </li>
                                </ul>
                            </div>
                        </div>-->
                        <div class="col-lg-12 pr-50 md-pr-15">
                            <div class="about-part">
                                <div class="sec-title mb-40">
                                    <div class="sub-title primary wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">About Us</div>
                                    <h2 class="title wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">KULIN - Kuliah & Kursus Online</h2>
                                    <div class="desc wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">Lorem ipsum dolor sit amet, consectetur adipisicing elit, eius to mod tempor incidi dunt ut labore et dolore magna aliqua. Ut enims ad minim veniam.Lorem ipsum dolor sit amet, consectetur adipisicing elit, eius to mod tempor incidi dunt ut labore et dolore magna aliqua. Ut enims ad minim veniam.Lorem sum dolor sit amet, consectetur adipisicing.</div>
                                </div>
                                <!-- <div class="sign-part wow fadeInUp" data-wow-delay="600ms" data-wow-duration="2000ms">
                                    <div class="img-part">
                                        <img src="assets/images/about/ceo.png" alt="CEO Image">
                                    </div>
                                    <div class="author-part">
                                        <span class="sign mb-10"><img src="assets/images/about/sign.png" alt="Sign"></span>
                                        <span class="post">CEO & Founder of Educavo</span>
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- About Section End -->
             <!-- Events Section Start -->
            <div class="rs-event home12style pt-0">
                <div class="container">
                    <div class="sec-title4 text-center mb-50">
                        <!--<div class="sub-title">Latest Course</div>-->
                        <h2 class="title purple-color">Latest Courses</h2>
                    </div> 
                    <div class="rs-carousel owl-carousel" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-autoplay-timeout="7000" data-smart-speed="2000" data-dots="true" data-nav="false" data-nav-speed="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="true" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="true" data-ipad-device2="1" data-ipad-device-nav2="false" data-ipad-device-dots2="true" data-md-device="3" data-md-device-nav="false" data-md-device-dots="true">
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/1.jpg" alt="Image">
                           </div>
                           <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/2.jpg" alt="Image">
                           </div>
                           <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/3.jpg" alt="Image">
                           </div>
                          <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/4.jpg" alt="Image">
                           </div>
                           <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/5.jpg" alt="Image">
                           </div>
                          <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/1.jpg" alt="Image">
                           </div>
                         <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                    <div class="event-item home12-style">
                        <div class="event-short">
                           <div class="featured-img">
                               <img src="assets/images/event/home12/2.jpg" alt="Image">
                           </div>
                          <div class="content-part">
                                <div class="all-dates-time">
                                    <div class="address"><i class="fa fa-money"></i> 60K</div>
                                   <div class="address"><i class="fa fa-file"></i> 6 Lessons</div>
                                </div>
                               <h4 class="title"><a href="#">Educational Technology and Mobile Learning</a></h4>
                               <div class="event-btm">
                                   <div class="date-part">
                                       <div class="date">
                                           <i class="fa fa-calendar-check-o"></i>
                                           July 24, 2020 
                                       </div>
                                   </div>
                                   <div class="btn-part">
                                       <a href="#">Enroll</a>
                                   </div>
                               </div>
                           </div> 
                        </div>
                    </div>
                  </div>
                </div> 
            </div>
            <!-- Events Section End -->
 <!-- Categories Section Start -->
            <div id="rs-categories" class="rs-categories gray-bg style1 pt-94 pb-70 md-pt-64 md-pb-40">
                <div class="container">
                    <div class="row y-middle mb-50 md-mb-30">
                        <div class="col-md-6 sm-mb-30">
                            <div class="sec-title">
                                <div class="sub-title primary">Subject Categories</div>
                                <h2 class="title mb-0">Our Top Categories </h2>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-part text-end sm-text-start">
                                <a href="#" class="readon">View All Categories</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                   <img src="assets/images/categories/icons/1.png" alt=""> 
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Genarel Education</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/2.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Computer Science</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/3.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Civil Engineering</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/4.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Artificial Intelligence</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/5.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Business Studies</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/6.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Web Development</h4>
                                    <span class="courses">05 Courses</span>
                                </div>
                            </a>
                        </div>  
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/7.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Life Course</h4>
                                    <span class="courses">8 Courses</span>
                                </div>
                            </a>
                        </div>  
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/8.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Lawyer Course</h4>
                                    <span class="courses">1 Courses</span>
                                </div>
                            </a>
                        </div>  
                        <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                            <a class="categories-item" href="#">
                                <div class="icon-part">
                                    <img src="assets/images/categories/icons/9.png" alt="">
                                </div>
                                <div class="content-part">
                                    <h4 class="title">Recipes</h4>
                                    <span class="courses">7 Courses</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Categories Section End -->
             <!-- Courses Section Start -->
            <div id="rs-popular-courses" class="rs-popular-courses main-home home12-style pt-90 pb-100 md-pt-40 md-pb-0">
                <div class="container">
                    <div class="sec-title4 text-center mb-45">
                        <div class="sub-title">Select Courses</div>
                        <h2 class="title black-color">Explore Popular Courses</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/1.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                                  
                                                  
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">Free</span>
                                        </div>
                                        <h3 class="title"><a href="#">Fitness Development Strategy Buildup Laoreet</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/2.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                               
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">$40.00</span>
                                        </div>
                                        <h3 class="title"><a href="#">Artificial Intelligence Fundamental Startup.</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 mb-30">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/3.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                              
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">$35.00</span>
                                        </div>
                                        <h3 class="title"><a href="#">Computer Science Startup et Commodo.</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 md-mb-30">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/4.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                              
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">$32.00</span>
                                        </div>
                                        <h3 class="title"><a href="#">Testy & Delicious Food Recipes for Lunch Tellus</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6 sm-mb-30">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/5.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                                  
                                                 
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">$22.00</span>
                                        </div>
                                        <h3 class="title"><a href="#">Lawyer Advance Mental Simulator Handle Nulla</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div> 
                        <div class="col-lg-4 col-md-6">
                           <div class="courses-item">
                                <div class="courses-grid">
                                    <div class="img-part">
                                        <a href="#"><img src="assets/images/courses/home12/6.jpg" alt=""></a>
                                    </div>
                                    <div class="content-part">
                                        <div class="info-meta">
                                            <ul>                                                
                                                <li class="ratings">
                                                  
                                                 
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="course-price">
                                            <span class="price">$28.00</span>
                                        </div>
                                        <h3 class="title"><a href="#">Computer Fundamentals Basic Startup Ultricies</a></h3>
                                        <ul class="meta-part">
                                            <li class="user">
                                                <i class="fa fa-user"></i>
                                                 25 Students                                        
                                            </li>
                                            <li class="user">
                                                <i class="fa fa-file"></i>
                                                6 Lessons                                        
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Courses Section End -->

 <!-- Choose Section Start -->
            <div class="why-choose-us style3">
                <div class="container">
                   <div class="row align-items-center">
                       <div class="col-lg-6 js-tilt md-mb-40">
                           <div class="img-part">
                                <img src="assets/images/choose/home12/1.png" alt="">
                            
                           </div>
                       </div>
                       <div class="col-lg-6 pl-60 md-pl-15">
                            <div class="sec-title3 mb-30">
                                <h2 class=" title new-title margin-0 pb-15">Why Learn Here</h2>
                                <div class="new-desc">Lorem ipsum dolor sit amet, ing elit, sed eius to mod tempors incididunt ut labore et dolore magna this aliqua.</div>
                            </div>
                            <div class="services-part mb-20">
                                <div class="services-icon">
                                    <img src="assets/images/choose/home12/icon/1.png" alt="">
                                </div>
                                <div class="services-text">
                                    <h2 class="title"> Lower Learning Cost</h2>
                                    <p class="services-txt">  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.</p>
                                </div>
                            </div> 
                            <div class="services-part mb-20">
                                <div class="services-icon">
                                    <img src="assets/images/choose/home12/icon/2.png" alt="">
                                </div>
                                <div class="services-text">
                                    <h2 class="title"> Learn With Experts</h2>
                                    <p class="services-txt">  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.</p>
                                </div>
                            </div> 
                            <div class="services-part">
                                <div class="services-icon">
                                    <img src="assets/images/choose/home12/icon/3.png" alt="">
                                </div>
                                <div class="services-text">
                                    <h2 class="title">Different Course Variation</h2>
                                    <p class="services-txt">  At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.</p>
                                </div>
                            </div>
                       </div> 
                   </div> 
                </div>
            </div>
            <!-- Choose Section End -->

            <!-- Counter Section End -->
            <div class="rs-counter home12-style pt-80">
                <div class="container">
                    <div class="row couter-area bg8">
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="counter-item text-center">
                                <h2 class="rs-count pr-0">50</h2>
                                <span class="prefix">k</span>
                                <h4 class="title mb-0">Enroll Learners</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 md-mb-30">
                            <div class="counter-item text-center">
                                <h2 class="number rs-count kplus">70</h2>
                                <h4 class="title mb-0">Enroll Learners</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 sm-mb-30">
                            <div class="counter-item text-center">
                                <h2 class="rs-count plus">120</h2>
                                <h4 class="title mb-0">Online Instructors</h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="counter-item text-center">
                                <h2 class="rs-count percent">99</h2>
                                <h4 class="title mb-0">Satisfaction Rate</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Counter Section Start -->
        
            <!-- Faq Section Start 
            <div class="rs-faq-part style1 pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 padding-0">
                          <div class="main-part">
                            <div class="title mb-40 md-mb-15">
                                <h2 class="text-part">Frequently Asked Questions</h2>
                            </div>
                              <div class="faq-content">
                                  <div class="accordion" id="accordionExample">
                                        <div class="accordion-item card">
                                            <div class="accordion-header card-header" id="headingOne">
                                                <button class="accordion-button card-link" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">What are the requirements ?</button>
                                            </div>
                                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                                <div class="accordion-body card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                                            </div>
                                        </div>
                                        <div class="accordion-item card">
                                            <div class="accordion-header card-header" id="headingTwo">
                                                <button class="card-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Does Educavo offer free courses?</button>
                                            </div>
                                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                                <div class="accordion-body card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                                            </div>
                                        </div>
                                        <div class="accordion-item card">
                                            <div class="accordion-header card-header" id="headingThree">
                                                <button class="card-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">What is the transfer application process?</button>
                                            </div>
                                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                                <div class="accordion-body card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                                            </div>
                                        </div>
                                        <div class="accordion-item card">
                                            <div class="accordion-header card-header" id="headingFour">
                                                <button class="card-link accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">What is distance education?</button>
                                            </div>
                                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                                <div class="accordion-body card-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                                            </div>
                                        </div>
                                    </div>
                              </div>
                          </div>
                        </div>
                        <div class="col-lg-6 padding-0">
                            <div class="img-part media-icon">
                                <a class="popup-videos" href="https://www.youtube.com/watch?v=atMUy_bPoQI">
                                    <i class="fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            faq Section Start -->

            <!-- Team Section Start
            <div id="rs-team" class="rs-team style1 gray-bg pt-94 pb-100 md-pt-64 md-pb-70">
                <div class="container">
                    <div class="sec-title mb-50 md-mb-30">
                        <div class="sub-title primary">Instructor</div>
                        <h2 class="title mb-0">Expert Teachers</h2>
                    </div>
                    <div class="rs-carousel owl-carousel nav-style2" data-loop="true" data-items="3" data-margin="30" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="true" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="2" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="2" data-ipad-device-nav2="false" data-ipad-device-dots2="false" data-md-device="3" data-md-device-nav="true" data-md-device-dots="false">
                        <div class="team-item">
                            <img src="assets/images/team/1.jpg" alt="">
                            <div class="content-part">
                                <h4 class="name"><a href="team-single.html">Jhon Pedrocas</a></h4>
                                <span class="designation">Professor</span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-item">
                            <img src="assets/images/team/2.jpg" alt="">
                            <div class="content-part">
                                <h4 class="name"><a href="team-single.html">Jesika Albenian</a></h4>
                                <span class="designation">Professor</span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="team-item">
                            <img src="assets/images/team/3.jpg" alt="">
                            <div class="content-part">
                                <h4 class="name"><a href="team-single.html">Alex Anthony</a></h4>
                                <span class="designation">Professor</span>
                                <ul class="social-links">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             Team Section End -->


            <!-- Section bg Wrap 2 start -->
            <div class="bg2">
                <!-- Blog Section Start 
                <div id="rs-blog" class="rs-blog style1 pt-94 pb-100 md-pt-64 md-pb-70">
                    <div class="container">
                        <div class="sec-title mb-60 md-mb-30 text-center">
                            <div class="sub-title primary">News Update </div>
                            <h2 class="title mb-0">Latest News & Events</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-7 col-md-12 pr-75 md-pr-15 md-mb-50">
                                <div class="row align-items-center no-gutter white-bg blog-item mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                                    <div class="col-md-6">
                                        <div class="image-part">
                                            <a href="#"><img src="assets/images/blog/1.jpg" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="blog-content">
                                            <ul class="blog-meta">
                                                <li><i class="fa fa-user-o"></i> Admin</li>
                                                <li><i class="fa fa-calendar"></i>June 15, 2019</li>
                                            </ul>
                                            <h3 class="title"><a href="#">University while the lovely valley team work </a></h3>
                                            <div class="btn-part">
                                                <a class="readon-arrow" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center no-gutter white-bg blog-item mb-30 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">
                                    <div class="col-md-6">
                                        <div class="image-part">
                                            <a href="#"><img src="assets/images/blog/2.jpg" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="blog-content">
                                            <ul class="blog-meta">
                                                <li><i class="fa fa-user-o"></i> Admin</li>
                                                <li><i class="fa fa-calendar"></i>June 15, 2019</li>
                                            </ul>
                                            <h3 class="title"><a href="#">While The Lovely Valley Team Work</a></h3>
                                            <div class="btn-part">
                                                <a class="readon-arrow" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center no-gutter white-bg blog-item wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                                    <div class="col-md-6">
                                        <div class="image-part">
                                            <a href="#"><img src="assets/images/blog/3.jpg" alt=""></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="blog-content">
                                            <ul class="blog-meta">
                                                <li><i class="fa fa-user-o"></i> Admin</li>
                                                <li><i class="fa fa-calendar"></i>June 15, 2019</li>
                                            </ul>
                                            <h3 class="title"><a href="#">Modern School The Lovely Valley Team Work</a></h3>
                                            <div class="btn-part">
                                                <a class="readon-arrow" href="#">Read More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 lg-pl-0">
                                <div class="events-short mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                                    <div class="date-part">
                                        <span class="month">June</span>
                                        <div class="date">20</div>
                                    </div>
                                    <div class="content-part">
                                        <div class="categorie">
                                            <a href="#">Math</a> & <a href="#">English</a>
                                        </div>
                                        <h4 class="title mb-0"><a href="#">Educational Technology and Mobile Learning</a></h4>
                                    </div>
                                </div>
                                <div class="events-short mb-30 wow fadeInUp" data-wow-delay="400ms" data-wow-duration="2000ms">
                                    <div class="date-part">
                                        <span class="month">June</span>
                                        <div class="date">21</div>
                                    </div>
                                    <div class="content-part">
                                        <div class="categorie">
                                            <a href="#">Math</a> & <a href="#">English</a>
                                        </div>
                                        <h4 class="title mb-0"><a href="#">Educational Technology and Mobile Learning</a></h4>
                                    </div>
                                </div>
                                <div class="events-short mb-30 wow fadeInUp" data-wow-delay="500ms" data-wow-duration="2000ms">
                                    <div class="date-part">
                                        <span class="month">June</span>
                                        <div class="date">22</div>
                                    </div>
                                    <div class="content-part">
                                        <div class="categorie">
                                            <a href="#">Math</a> & <a href="#">English</a>
                                        </div>
                                        <h4 class="title mb-0"><a href="#">Educational Technology and Mobile Learning</a></h4>
                                    </div>
                                </div>
                                <div class="events-short wow fadeInUp" data-wow-delay="600ms" data-wow-duration="2000ms">
                                    <div class="date-part">
                                        <span class="month">June</span>
                                        <div class="date">23</div>
                                    </div>
                                    <div class="content-part">
                                        <div class="categorie">
                                            <a href="#">Math</a> & <a href="#">English</a>
                                        </div>
                                        <h4 class="title mb-0"><a href="#">Educational Technology and Mobile Learning</a></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->
                <!-- Blog Section End -->

                <!-- Newsletter section start
                <div class="rs-newsletter style1 mb--124 sm-mb-0 sm-pb-70">
                    <div class="container">
                        <div class="newsletter-wrap">
                            <div class="row y-middle">
                                <div class="col-md-6 sm-mb-30">
                                    <div class="sec-title">
                                        <div class="sub-title white-color">Newsletter</div>
                                        <h2 class="title mb-0 white-color">Subscribe Us to join <br> Our Community </h2>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <form class="newsletter-form">
                                        <input type="email" name="email" placeholder="Enter Your Email" required="">
                                        <button type="submit">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!-- Newsletter section end -->
            </div>
            <!-- Section bg Wrap 2 End -->
        </div> 
        <!-- Main content End -->

  <!-- Download Section Start -->
            <div class="rs-download-app pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                   <div class="row align-items-center">
                       <div class="col-lg-6 md-mb-40">
                           <div class="img-part">
                                <img src="assets/images/download/m-app.png" alt="">
                            
                           </div>
                       </div>
                       <div class="col-lg-6 pl-60 md-pl-15">
                            <div class="sec-title3 mb-30">
                                <div class="sub-title green-color">Download Mobile App</div>
                                <h2 class=" title new-title">Learn anything - Any Time - Any Where ; Mobile/Desktop</h2>
                                <div class="new-desc">Lorem ipsum dolor sit amet, ing elit, sed eius to mod tempors incididunt ut labore et dolore magna this aliqua sed eius to mod tempors incid idunt ut labore data management.</div>
                            </div>
                            <div class="mobile-img">
                                <div class="apps-image pr-20 sm-pr-5">
                                    <img src="assets/images/download/play.png" alt="">
                                </div>
                                <!--<div class="apps-image">
                                    <img src="assets/images/download/apple.png" alt="">
                                </div>-->
                            </div>
                       </div> 
                   </div> 
                </div>
            </div>
            <!-- Download Section End -->

            <?php
include  "footer.php";
?>