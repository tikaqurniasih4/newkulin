<?php
include  "header.php";
?>
<!-- Breadcrumbs Start -->
<!-- <div class="rs-breadcrumbs breadcrumbs-overlay">
  <div class="breadcrumbs-img">
    <img src="assets/images/breadcrumbs/1.jpg" alt="Breadcrumbs Image">
  </div>
  <div class="breadcrumbs-text">
    <h1 class="page-title">Checkout</h1>
    <ul>
      <li>
        <a class="active" href="index.html">Home</a>
      </li>
      <li>Checkout</li>
    </ul>
  </div>
</div> -->
<!-- Breadcrumbs End -->
<!-- Intro Courses -->
<section class="intro-section gray-bg pt-94 pb-100 md-pt-64 md-pb-70">
  <div class="container">
    <div class="row">
      <!-- Content Column -->
      <div class="col-lg-8 md-mb-50">
        <!-- Intro Info Tabs-->
              <div class="content white-bg pt-30 pb-30">
                <!-- Cource Overview -->
                <!-- Cart Section Start -->
               
                  <div class="container">
                    <div class="cart-wrap">
                    <h3 class="instructor-title">Pilih Tanggal Kursus</h3>
                      <!-- partial:index.partial.html -->
                      <div class="app-container" ng-app="dateTimeApp" ng-controller="dateTimeCtrl as ctrl" ng-cloak>
                        <div date-picker datepicker-title="Select Date" picktime="true" pickdate="true" pickpast="false" mondayfirst="false" custom-message="You have selected" selecteddate="ctrl.selected_date" updatefn="ctrl.updateDate(newdate)">
                          <div class="datepicker" ng-class="{
                    'am': timeframe == 'am',
                    'pm': timeframe == 'pm',
                    'compact': compact
                }">
                            <div class="datepicker-header">
                              <div class="datepicker-title" ng-if="datepicker_title"></div>
                              <div class="datepicker-subheader">{{ customMessage }} {{ selectedDay }} {{ monthNames[localdate.getMonth()] }} {{ localdate.getDate() }}, {{ localdate.getFullYear() }}</div>
                            </div>
                            <div class="datepicker-calendar">
                              <div class="calendar-header">
                                <div class="goback" ng-click="moveBack()" ng-if="pickdate">
                                  <svg width="30" height="30">
                                    <path fill="none" stroke="#0DAD83" stroke-width="3" d="M19,6 l-9,9 l9,9" />
                                  </svg>
                                </div>
                                <div class="current-month-container">{{ currentViewDate.getFullYear() }} {{ currentMonthName() }}</div>
                                <div class="goforward" ng-click="moveForward()" ng-if="pickdate">
                                  <svg width="30" height="30">
                                    <path fill="none" stroke="#0DAD83" stroke-width="3" d="M11,6 l9,9 l-9,9" />
                                  </svg>
                                </div>
                              </div>
                              <div class="calendar-day-header">
                                <span ng-repeat="day in days" class="day-label">{{ day.short }}</span>
                              </div>
                              <div class="calendar-grid" ng-class="{false: 'no-hover'}[pickdate]">
                                <div ng-class="{'no-hover': !day.showday}" ng-repeat="day in month" class="datecontainer" ng-style="{'margin-left': calcOffset(day, $index)}" track by $index>
                                  <div class="datenumber" ng-class="{'day-selected': day.selected }" ng-click="selectDate(day)">
                                    {{ day.daydate }}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="timepicker" ng-if="picktime == 'true'">
                              <div ng-class="{'am': timeframe == 'am', 'pm': timeframe == 'pm' }">
                                <div class="timepicker-container-outer" selectedtime="time" timetravel>
                                  <div class="timepicker-container-inner">
                                    <div class="timeline-container" ng-mousedown="timeSelectStart($event)" sm-touchstart="timeSelectStart($event)">
                                      <div class="current-time">
                                        <div class="actual-time">{{ time }}</div>
                                      </div>
                                      <div class="timeline"></div>
                                      <div class="hours-container">
                                        <div class="hour-mark" ng-repeat="hour in getHours() track by $index"></div>
                                      </div>
                                    </div>
                                    <div class="display-time">
                                      <div class="decrement-time" ng-click="adjustTime('decrease')">
                                        <svg width="24" height="24">
                                          <path stroke="white" stroke-width="2" d="M8,12 h8" />
                                        </svg>
                                      </div>
                                      <div class="time" ng-class="{'time-active': edittime.active}">
                                        <input type="text" class="time-input" ng-model="edittime.input" ng-keydown="changeInputTime($event)" ng-focus="edittime.active = true; edittime.digits = [];" ng-blur="edittime.active = false" />
                                        <div class="formatted-time">{{ edittime.formatted }}</div>
                                      </div>
                                      <div class="increment-time" ng-click="adjustTime('increase')">
                                        <svg width="24" height="24">
                                          <path stroke="white" stroke-width="2" d="M12,7 v10 M7,12 h10" />
                                        </svg>
                                      </div>
                                    </div>
                                    <div class="am-pm-container">
                                      <div class="am-pm-button" ng-click="changetime('am');">am</div>
                                      <div class="am-pm-button" ng-click="changetime('pm');">pm</div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="buttons-container">
                              <!-- <div class="cancel-button">CANCEL</div> -->
                              <div class="save-button">SELECT</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                
                <!-- Cart Section End -->
              </div>
            </div>



            <!-- Content Column -->
      <div class="col-lg-4 md-mb-50">
        <!-- Intro Info Tabs-->
              <div class="content white-bg pt-30">
                <!-- Cource Overview -->
                <!-- Cart Section Start -->

                  <div class="container">
                    <div class="cart-wrap">
                  
                                            <h3 class="instructor-title">Pilih Tutor</h3>
                                            <div class="row rs-team style1 orange-color transparent-bg clearfix">
                                                <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                                                    <div class="team-item">
                                                        <img src="assets/images/team/1.jpg" alt="">
                                                        <div class="content-part">
                                                        <h6 class="name"><a href="#">Jhon Pedrocas</a></h6>
                                                            <span class="designation">Professor</span>
                                                            
                                                        </div>
                                                    </div>
                                                </div>                                                            
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="team-item">
                                                        <img src="assets/images/team/2.jpg" alt="">
                                                        <div class="content-part">
                                                        <h6 class="name"><a href="#">Jhon Pedrocas</a></h6>
                                                            <span class="designation">Professor</span>
                                                          
                                                           
                                                        </div>
                                                    </div>
                                                </div>                                                            
                                            </div>  
                                      
                    </div>
                  </div>
                
                <!-- Cart Section End -->
              </div>
            </div>
          </div>
        </div>
    
    
</section>
<!-- End intro Courses -->




</div>
<!-- Main content End -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.5.2/angular.min.js'></script>
<script src="./script.js"></script> <?php
include  "footer.php";
?>